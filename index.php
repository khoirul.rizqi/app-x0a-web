<?php 
require 'function.php';
$mahasiswa = query("Select * from mahasiswa join prodi on mahasiswa.prodi=prodi.id_prodi");
?>

<!DOCTYPE html>
<html>
<head>
	<title>INDEX</title>
</head>
<h1>Daftar Mahasiswa</h1>
<a href="tambah.php"> + Tambah Data</a><br><br>
<body>
	<table border="1" cellpadding="10" cellspacing="0">
		<tr>
			<td>No.</td>
			<td>Nim	</td>
			<td>Nama</td>
			<td>Prodi</td>
			<td>Foto</td>
			<td>TTl</td>
			<td>Alamat</td>
			<td>Jenis Kelamin</td>
			<td>Aksi</td>
		</tr>
		<?php $i=1; ?>
		<?php foreach ($mahasiswa as $a) : ?>
			<tr>
				<td><?= $i; ?></td>
				<td><?= $a["nim"]; ?></td>
				<td><?= $a["nama"]; ?></td>
				<td><?= $a["nama_prodi"]; ?></td>
				<td><img src="images/<?= $a["foto"]; ?>" height="80px" width="80px"></td>
				<td><?= $a["ttl"]; ?></td>
				<td><?= $a["alamat"]; ?></td>
				<td><?= $a["jk"]; ?></td>
				<td><a href="ubah.php?nim=<?= $a["nim"]; ?>">Ubah</a> | <a href="hapus.php?nim=<?= $a["nim"]; ?>" onClick="return confirm('yakin');">Delete</a></td>
			</tr>
			<?php $i++; ?>
		<?php endforeach; ?>
	</table>
</body>
</html>