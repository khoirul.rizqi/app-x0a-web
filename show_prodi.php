<?php
	$DB_NAME = "kampus";
	$DB_USER = "root";
	$DB_PASS = "";
	$DB_SERVER_LOC = "localhost";

	if ($_SERVER['REQUEST_METHOD']=='POST') {
		$conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
		$sql = "SELECT id_prodi, nama_prodi FROM prodi";
		$result = mysqli_query($conn,$sql);
		$x = mysqli_num_rows($result);
		if($x>0){
			header("Access-Control-Allow-Origin: *");
			header("Content-type: application/json; charset=UTF-8");
			$data_pro = array();
			while($pro = mysqli_fetch_assoc($result)){
				array_push($data_pro, $pro);
			}
			echo json_encode($data_pro);
		}
	}
?>