<?php 
require 'function.php';
$prodi = query("select * from prodi");

if (isset($_POST['submit'])) {
	//var_dump($_POST);
	//var_dump($_FILES);
	//die();
	if (tambah($_POST)>0) {
		echo "
		<script>
		alert('data berhasil ditambahkan');
		document.location.href='index.php';
		</script>
		";
	}else {
		echo "
		<script>
		alert('data gagal ditambahkan');
		document.location.href='index.php';
		</script>
		";
	}
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>TAMBAH</title>
</head>
<body>
	<h1>Tambah Data Mahasiswa</h1>
	<a href="index.php"> + kembali ke index </a><br><br>
	<form action="" method="POST" enctype="multipart/form-data">
		<table border="0">
			<tr>
				<td><label for="1">Nim</label></td>
				<td>:</td>
				<td><input type="text" name="nim" id="1"></td>
			</tr>
			<tr>
				<td><label for="2">Nama</label></td>
				<td>:</td>
				<td><input type="text" name="nama" id="2"></td>
			</tr>
			<tr>
				<td><label for="3">Prodi</label></td>
				<td>:</td>
				<td>
					<select id="3" name="prodi">
						<?php foreach ($prodi as $a) : ?>
							<option value="<?= $a['id_prodi']; ?>"><?= $a['nama_prodi'] ?></option>
						<?php endforeach; ?>
					</select >
				</td>
			</tr>
			<tr>
				<td><label for="4">Foto</label></td>
				<td>:</td>
				<td><input type="file" name="foto" id="4"></td>
			</tr>
			<tr>
				<td><label for="5">TTL</label></td>
				<td>:</td>
				<td><input type="text" name="ttl" id="5"></td>
			</tr>
			<tr>
				<td><label for="6">Alamat</label></td>
				<td>:</td>
				<td><input type="text" name="alamat" id="6"></td>
			</tr>
			<tr>
				<td><label for="7">Jenis Kelamin</label></td>
				<td>:</td>
				<td>
					<label><input type="radio" name="jk" value="laki-laki" /> Laki-laki</label>
					<label><input type="radio" name="jk" value="perempuan" /> Perempuan</label>
				</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><button type="submit" name="submit">Tambah Data</button></td>
			</tr>
		</table>
	</form>
</body>
</html>