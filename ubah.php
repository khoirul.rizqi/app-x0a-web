<?php 
require 'function.php';
$nim=$_GET['nim'];
$m = query("Select * from mahasiswa join prodi on mahasiswa.prodi=prodi.id_prodi where nim='$nim'")[0];
$prodi = query("select * from prodi");
if (isset($_POST['submit'])) {
	if (ubah($_POST)>0) {
		echo "
		<script>
		alert('data berhasil diubah');
		document.location.href='index.php';
		</script>
		";
	}else {
		echo "
		<script>
		alert('data gagal diubah');
		document.location.href='index.php';
		</script>
		";
	}
}

?>

<!DOCTYPE html>
<html>
<head>
	<title>UBAH</title>
</head>
<body>
	<h1>Ubah Data Mahasiswa</h1>
	<a href="index.php"> + kembali ke index </a><br><br>
	<form action="" method="POST" enctype="multipart/form-data">
		<table border="0">
			<input type="" name="">
			<tr>
				<td><label for="1">Nim</label></td>
				<td>:</td>
				<td><input type="text" name="nim" id="1" value="<?= $m['nim'] ?>"></td>
			</tr>
			<tr>
				<td><label for="2">Nama</label></td>
				<td>:</td>
				<td><input type="text" name="nama" id="2" value="<?= $m['nama'] ?>"></td>
			</tr>
			<tr>
				<td><label for="3">Prodi</label></td>
				<td>:</td>
				<td>
					<select id="3" name="prodi">
						<?php foreach ($prodi as $a) : ?>
							<option selected="2" value="<?= $a['id_prodi']; ?>"><?= $a['nama_prodi'] ?></option>
						<?php endforeach; ?>
					</select >
				</td>
			</tr>
			<tr>
				<td><label for="4">Foto</label></td>
				<td>:</td>
				<td><input type="file" name="foto" id="4"></td>
			</tr>
			<tr>
				<td><label for="5">TTL</label></td>
				<td>:</td>
				<td><input type="date" name="ttl" id="5" value="<?= $m['ttl']; ?>"></td>
			</tr>
			<tr>
				<td><label for="6">Alamat</label></td>
				<td>:</td>
				<td><input type="text" name="alamat" id="6" value="<?= $m['alamat']; ?>"></td>
			</tr>
			<tr>
				<td><label for="7">Jenis Kelamin</label></td>
				<td>:</td>
				<td>
					<label><input type="radio" name="jk" value="laki-laki" /> Laki-laki</label>
					<label><input type="radio" name="jk" value="perempuan" /> Perempuan</label>
				</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><button type="submit" name="submit">Ubah Data</button></td>
			</tr>
		</table>
	</form>
</body>
</html>